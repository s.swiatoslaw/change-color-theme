let button = document.querySelector('.button');
let hrefNew = './css/main-theme.css';
let hrefOld = './css/main.css';
let hrefCheck = document.getElementById('style').getAttribute('href');
    
button.addEventListener('click', function (){
        if (hrefCheck == hrefOld) {
            document.getElementById('style').href = hrefNew;
            hrefCheck = './css/main-theme.css';  
            localStorage.setItem('styleSaved', hrefCheck); 
            console.log(hrefCheck)
        }
         else if (hrefCheck == hrefNew) {
            document.getElementById('style').href = hrefOld; 
            hrefCheck = './css/main.css';
            localStorage.setItem('styleSaved', hrefCheck); 
            console.log(hrefCheck)
        }
    })

let themeStorage = localStorage.getItem('styleSaved');
console.log(themeStorage)
document.getElementById('style').href = themeStorage;

    